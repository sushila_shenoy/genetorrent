#!/usr/bin/env python2.7
#
# Copyright (c) 2012, Regents of the University of California
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the University of California nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL REGENTS OF THE UNIVERSITY OF CALIFORNIA BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

''' Unit tests for download client '''
from contextlib import contextmanager
import string
import random

import unittest
import time
import os
import logging
import sys

from uuid import uuid4
from shutil import copy2
from xml.etree.cElementTree import Element, ElementTree, SubElement
from tempfile import NamedTemporaryFile
from utils.genetorrent import GeneTorrentInstance, InstanceType

from utils.gttestcase import GTTestCase, StreamToLogger
from utils.cgdata.datagen import DataGenZero
from utils.config import TestConfig

class TestGeneTorrentDownload(GTTestCase):
    create_mockhub = True
    create_credential = True

    def test_100mb_download_from_uuid(self):
        '''Download a 100MB zero-data file from a GT server.'''
        uuid = self.data_upload_test(1024 * 1024 * 100,
            data_generator=DataGenZero)
        self.data_download_test_uuid(uuid,
            client_options='--max-children=4 -r 1000.5', server_ct=2)

    def test_32mb_download_from_uuid(self):
        '''Download a randomly-generated 32MB file from a GT server.'''
        uuid = self.data_upload_test(1024 * 1024 * 32)
        self.data_download_test_uuid(uuid)

    def test_32mb_download_from_gto(self):
        '''Download a randomly-generated 32MB file from a GT server.'''
        # GTO downloads must be manually added to server workqueue
        # therefore, this test will not work remotely
        if not TestConfig.MOCKHUB:
            return
        uuid = self.data_upload_test(1024 * 1024 * 32)
        self.data_download_test_gto(uuid)

    def test_hinted_1mb_download_from_gto(self):
        if not TestConfig.MOCKHUB:
            return
        uuid = self.data_upload_test(1024 * 1024 * 1)
        self.data_download_test_gto(uuid, download_switch='--download-from-gto' )

    def _test_download_from_xml( self, num_uuids, size_in_mb,
                                 include_comment=False, include_declaration=False, hinted=False):
        uuids = [ self.data_upload_test( 1024 * 1024 * size_in_mb ) for _ in range( num_uuids ) ]
        result_set = Element( 'ResultSet' )
        for uuid in uuids:
            result = SubElement( result_set, 'Result' )
            analysis_data_uri = SubElement( result, 'analysis_data_uri' )
            analysis_data_uri.text = '%s/cghub/data/analysis/download/' % TestConfig.HUB_SERVER + str( uuid )
        doc = ElementTree( result_set )
        f = NamedTemporaryFile( delete=False, suffix='.xml' )
        if include_declaration: f.write( '<?xml version="1.0" ?>\n' )
        if include_comment: f.write( '<!-- foo -->\n' )
        doc.write( f )
        f.close( )
        self.data_download_test_xml( f.name,
                                     uuids,
                                     download_switch='--download-from-xml' if hinted else '--download' )
        os.remove( f.name )

    def test_3x32mb_download_from_xml( self ):
        '''Download three randomly-generated 32MB files from a GT server via an XML manifest'''
        self._test_download_from_xml( num_uuids=3, size_in_mb=32 )

    def test_download_from_xml_with_declaration( self ):
        self._test_download_from_xml( num_uuids=1, size_in_mb=1, include_declaration=True )

    def test_download_from_xml_with_comment( self ):
        self._test_download_from_xml( num_uuids=1, size_in_mb=1, include_comment=True )

    def test_download_from_xml_with_comment_and_declaration( self ):
        self._test_download_from_xml( num_uuids=1,
                                      size_in_mb=1,
                                      include_comment=True,
                                      include_declaration=True )

    def test_hinted_download_from_xml( self ):
        self._test_download_from_xml( num_uuids=1, size_in_mb=1, hinted=True )

    @staticmethod
    def random_string( min_size, max_size=None ):
        """
        >>> random.seed(0)
        >>> TestGeneTorrentDownload.random_string(0)
        ''
        >>> TestGeneTorrentDownload.random_string(1,1)
        'q'
        >>> TestGeneTorrentDownload.random_string(2,2)
        'k0'
        >>> TestGeneTorrentDownload.random_string(0,2)
        ''
        >>> TestGeneTorrentDownload.random_string(0,2)
        'Z'
        >>> TestGeneTorrentDownload.random_string(0,2)
        'S3'
        """
        if max_size is None:
            max_size = min_size
            min_size = 0
        if min_size == max_size:
            size = min_size
        elif min_size < max_size:
            size = random.randint( min_size, max_size )
        else:
            raise ValueError()
        return ''.join(
            random.choice( string.ascii_uppercase + string.digits + string.ascii_lowercase )
                for _ in range( size ) )

    @classmethod
    def generate_download_from_tsv_tests( cls, max_num_rows, max_num_columns, hinted=False ):
        """
        This is a method that generates test methods! The general goal is to test different TSV
        layouts while having the setup and tear-down happen before and after each layout.
        """
        random.seed( 42 )
        for num_columns in range( 1, max_num_columns + 1 ):
            for uuid_column_index in range( num_columns ):
                for num_rows in range( 1, max_num_rows + 1 ):
                    def test_method( self ):
                        def generate_row( uuid_column_value, is_header=False ):
                            row = [ self.random_string( 1 if is_header else 0, 20 )
                                for _ in range( num_columns - 1 ) ]
                            row.insert( uuid_column_index, str( uuid_column_value ) )
                            return '\t'.join( row ) + '\n'
                        uuids = [ self.data_upload_test( 1024 * 1024 ) for _ in range( num_rows ) ]
                        f = NamedTemporaryFile( delete=False, suffix='.tsv' )
                        header = generate_row( 'analysis_id', is_header=True )
                        sys.stderr.write( header )
                        f.write( header )
                        for uuid in uuids:
                            row = generate_row( uuid )
                            sys.stderr.write( row )
                            f.write( row )
                        f.close( )
                        mock_ws_url = '%s/cghub/data/analysis/download ' % TestConfig.HUB_SERVER
                        self.data_download_test_xml( f.name, uuids,
                                                     # when passing in bare UUIDs we must tell gtdownload where to
                                                     # get the GTO from, otherwise it would hit CGHub production
                                                     client_options='--webservices-url %s' % mock_ws_url,
                                                     download_switch='--download-from-tsv' if hinted else '--download' )
                        os.remove( f.name )
                    setattr( cls, 'test_download_from_tsv_%i_%i_%i' % ( num_columns, uuid_column_index, num_rows ), test_method )

    def _test_download_from_lst_file( self, max_num_rows , hinted=False):
        random.seed( 42 )
        for num_rows in range( 1, max_num_rows + 1 ):
            uuids = [ self.data_upload_test( 1024 * 1024 ) for _ in range( num_rows ) ]
            f = NamedTemporaryFile( delete=False, suffix='.lst' )
            for uuid in uuids:
                if random.randint( 0, 1 ) == 0:
                    row = '%s/cghub/data/analysis/download/%s\n' % ( TestConfig.HUB_SERVER, uuid )
                else:
                    row = str( uuid ) + '\n'
                sys.stderr.write( row )
                f.write( row )
            f.close( )
            mock_ws_url = '%s/cghub/data/analysis/download ' % TestConfig.HUB_SERVER
            self.data_download_test_xml( f.name, uuids,
                                         # when passing in bare UUIDs we must tell gtdownload where to
                                         # get the GTO from, otherwise it would hit CGHub production
                                         client_options='--webservices-url %s' % mock_ws_url,
                                         download_switch='--download-from-lst' if hinted else '--download' )
            os.remove( f.name )

    def test_download_from_lst_file(self):
        self._test_download_from_lst_file( 2 )

    def test_hinted_download_from_lst_file(self):
        self._test_download_from_lst_file( 1, hinted=True )

    @contextmanager
    def temp_file( self, contents, extension ):
        f = NamedTemporaryFile( delete=False, suffix=extension )
        f.write( contents )
        f.close( )
        yield f.name
        os.remove( f.name )

    def _test_noop_download_from_file( self, extension, contents, expected_status, expected_output ):
        with self.temp_file( 'dummy', '.pem' ) as token:
            with self.temp_file( contents, extension ) as tsv:
                client = GeneTorrentInstance( '-d %s -c %s' % ( tsv, token ),
                                              instance_type=InstanceType.GT_DOWNLOAD )
                stdout, stderr = client.communicate( )
                self.assertTrue( expected_output in stdout )
                self.assertEquals( client.returncode, expected_status )

    def _test_noop_download_from_tsv( self, contents, expected_status, expected_output ):
        self._test_noop_download_from_file( '.tsv', contents, expected_status, expected_output )

    def _test_noop_download_from_lst_file( self, contents, expected_status, expected_output ):
        self._test_noop_download_from_file( '', contents, expected_status, expected_output )

    def test_download_from_empty_tsv( self ):
        self._test_noop_download_from_tsv( 'analysis_id\n', 0, '' )

    def test_download_from_tsv_without_columns( self ):
        self._test_noop_download_from_tsv( '\n\n', 201, 'Cannot determine type of download argument' )

    def test_download_from_tsv_missing_analysis_id( self ):
        self._test_noop_download_from_tsv( 'foo\n', 201, 'Cannot determine type of download argument' )

    def test_download_from_tsv_with_long_row( self ):
        self._test_noop_download_from_tsv( 'foo\tanalysis_id\nbar\t0f710cbd-f85c-4a96-abc9-cd22ae202c4b\tbla\n', 97, 'TSV contains row with too few or too many columns' )

    def test_download_from_tsv_with_short_row( self ):
        self._test_noop_download_from_tsv( 'foo\tanalysis_id\n0f710cbd-f85c-4a96-abc9-cd22ae202c4b\n', 97, 'TSV contains row with too few or too many columns' )

    def test_download_from_tsv_with_invalid_uuid( self ):
        self._test_noop_download_from_tsv( 'foo\tanalysis_id\nfoo\tbar\n', 97, "'bar' is not a valid UUID" )

    def test_download_from_empty_lst_file(self):
        self._test_noop_download_from_lst_file( '', 0, '' )

    def test_download_from_lst_file_with_invalid_line(self):
        self._test_noop_download_from_lst_file( '0f710cbd-f85c-4a96-abc9-cd22ae202c4b\nfoo', 97, 'Unrecognized line in list file' )

TestGeneTorrentDownload.generate_download_from_tsv_tests( 2, 3 )

TestGeneTorrentDownload.generate_download_from_tsv_tests( 1, 1, hinted=True )



if __name__ == '__main__':
    sys.stdout = StreamToLogger(logging.getLogger('stdout'), logging.INFO)
    sys.stderr = StreamToLogger(logging.getLogger('stderr'), logging.WARN)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestGeneTorrentDownload)
    result = unittest.TextTestRunner(stream=sys.stderr, verbosity=2).run(suite)
    if not result.wasSuccessful():
        sys.exit(1)

